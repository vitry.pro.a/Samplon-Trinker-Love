import json
from pprint import pprint
from termcolor import colored
import statistics
import math

with open('people.json', 'r') as p:
    people = json.loads(p.read())

print(colored("""
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   
""", 'yellow'))
print(colored('Modele des données :', 'yellow'))
pprint(people[0])

# debut de l'exo
print(colored(''.join(['_' for _ in range(80)]), 'green', 'on_green'))

# Nombre de personnes dans le tableau
print("Nombre de personnes")
print(len(people))

print(colored("Nombre d'hommes : ", 'yellow'))
# pour chaque personne du tableau, si son genre == 'Male' je le met dans le tableau hommes
hommes1 = [p for p in people if p['gender'] == 'Male']
# len() revoie la taille (nombre d'élément) d'un tableau
print(len(hommes1))
print("M1 > Exemple")

################################################################################

# je peux aussi l'écrire avec une boucle classique
hommes2 = []                        # un tableau vide
for person in people:               # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme (2-266-02250-4)
        hommes2.append(person)      # je l'ajoute au tableau
pprint(len(hommes2))
print("M2 > Exemple")

################################################################################

# dans la même idée, plutot que de mettre tous les hommes dans un tableau
# puis afficher la longueur du tableau, je peux juste les compter dans une variable

def fc_nb_homme():
    nb_homme = 0                       # je commence à 0
    for person in people:               # pour chaque persone du tableau
        if person["gender"] == "Male":  # si c'est un homme
            nb_homme = nb_homme + 1   # j'ajoute 1 à mon compteur
    return nb_homme

pprint(fc_nb_homme())
print("M3 > Fonction")

################################################################################

print(colored("Nombre de femmes : ", 'yellow'))
# je peux compter les femmes ou calculer : nombre d'élement dans people - nombre d'homme
print(colored('Fait', 'red', 'on_green'))

def fc_nb_femme():
    nb_femme = 0                        # je commence à 0
    for person in people:                # pour chaque persone du tableau
        if person["gender"] == "Female": # si c'est une femme
            nb_femme = nb_femme + 1    # j'ajoute 1 à mon compteur
    return nb_femme

print(fc_nb_femme())
print("M1 > Fonction")

nb_femmes = len(people) - int(fc_nb_homme())  # méthode 2: par soustraction
pprint(nb_femmes)
print("M2 > Itératif")

################################################################################

print(colored("Nombre de personnes qui cherchent un homme :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

def fc_nb_search_M():
    nb_search_M = 0
    for person in people:                     # pour chaque persone du tableau
            if person["looking_for"] == "M":  # si la recherche est Homme
                nb_search_M = nb_search_M + 1 # j'ajoute 1 à mon compteur
    return nb_search_M

pprint(fc_nb_search_M())
print("Fonction")

################################################################################

print(colored("Nombre de personnes qui cherchent une femme :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

def fc_nb_search_F():
    nb_search_F = 0
    for person in people:                     # pour chaque persone du tableau
            if person["looking_for"] == "F":  # si la recherche est Femme
                nb_search_F = nb_search_F + 1 # j'ajoute 1 à mon compteur
    return nb_search_F

pprint(fc_nb_search_F())
print("Fonction")

################################################################################

print(colored("Nombre de personnes qui gagnent plus de 2000$ :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

def fc_nb_win_money(count: int):
    nb_win_money = 0
    income = []
    for person in people:                       # pour chaque persone du tableau
            income = person["income"]          # liste du salaire (actuel) dans une nouvelle liste
            if float(income[1:]) > count :     # si le gain (salaire - amputé du $) est supérieur à count
                nb_win_money = nb_win_money + 1 # j'ajoute 1 à mon compteur
    return nb_win_money
# le <person["income"]> actuel (boucle for) transforme une itération dans la liste
# <person> à la colonne <incone> en une liste <income> "revenue"
# puis on ampute le 1er caractère de la liste par <income[1:]>

pprint(fc_nb_win_money(2000))
print("Fonction avec argument")

################################################################################

print(colored("Nombre de personnes qui aiment les Drama :", 'yellow'))
# là il va falloir regarder si le chaine de charactères "Drama" se trouve dans "pref_movie"
print(colored('Fait', 'red', 'on_green'))

# Test trie des films drama
def drama():
    nb_search_drama1 = 0
    nb_search_drama2 = 0
    for person in people:
            truc = person["pref_movie"]
            for i in truc:
                if truc[:] == "Drama" :
                    nb_search_drama1 = nb_search_drama1 + 1
            if person["pref_movie"] == "Drama":
                nb_search_drama2 = nb_search_drama2 + 1
    print("Drama contenu dans chaine")
    print(nb_search_drama1)
    print("Drama complet")
    print(nb_search_drama2)

print(colored('Méthode 1: NOK 471 bon nonbre', 'red', 'on_green'))
# Méthode 1: Fonction de comptage et de recherche spécifique
def fc_nb_search_drama():
    nb_search_drama = 0                                      # 1 init: nb recherche
    for person in people:                                    # 2 parcours liste people
        list_movie = person["pref_movie"]                    # 2.1: init: liste film
        for i in list_movie:                                 # 2.2: parcours liste film
            if list_movie[:] == "Drama" :                    # 3 si pref_movie = drama
                nb_search_drama = nb_search_drama + 1        # 5 incrémentation compteur
    return nb_search_drama                                   # 6 retour du nb drama

pprint(fc_nb_search_drama())
print("Fonction spécifique")

print(colored('Méthode 2: Echec', 'red', 'on_yellow'))
# Méthode 2: Fonction générique de comptage et de recherche
def fc_nb_scearch_bis(type, scearch):
    nb_scearch = 0                                # 1
    list_scearch = fc_scearch_bis(type, scearch)   # 2 Appel fonction, puis stockage dans une liste
    for current_scearch in list_scearch:          # 4.3: Parcours de la liste
        if list_scearch[current_scearch] == True: # 4.4: Vérification état: si True
            nb_scearch = nb_scearch + 1           # 5: Incrémentation du nombre de recherche
    return nb_scearch                             # 6

def fc_scearch_bis(type, scearch):
    for person in people:                         # 2
        list_movie = person[type]                 # 2.1
        for i in list_movie:                      # 2.2
            if list_movie[:] == scearch:          # 3
                return True                       # 4.2: Retour Etat de la recherche
            else:
                return False                      # 4.2: Retour Etat de la recherche

#print(fc_nb_scearch_bis("pref_movie", "Drama"))
print("Fonction générique")

################################################################################

print(colored("Nombre de femmes qui aiment la science-fiction :", 'yellow'))
# si j'ai déjà un tableau avec toutes les femmes, je peux chercher directement dedans ;)
print(colored('Fait', 'red', 'on_green'))

# Méthode 1: Fonction de comptage et de recherche spécifique
def fc_nb_search_SF_F():
    nb_search_SF_F = 0
    for person in people:
        if person["gender"] == "Female":
            list_movie = person["pref_movie"]
            if list_movie == "Sci-Fi" :
                nb_search_SF_F = nb_search_SF_F + 1  
    return nb_search_SF_F

pprint(fc_nb_search_SF_F())
print("Fonction spécifique")

# Méthode 2: Fonction compact
def fc_nb_search_SF_F():
    nb_search_SF_F = 0
    for person in people:
        if person["gender"] == "Female" and person["pref_movie"] == "Sci-Fi":
            nb_search_SF_F = nb_search_SF_F + 1  
    return nb_search_SF_F

pprint(fc_nb_search_SF_F())
print("Fonction spécifique compact")

################################################################################

print(colored('LEVEL 2' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# Méthode 1: Fonction de comptage et de recherche spécifique
def fc_nb_search_D_income():
    nb_search_D_income = 0
    for person in people:
        income = person["income"]
        list_movie = person["pref_movie"]
        if float(income[1:]) > 1482 and list_movie == "Documentary" :
            nb_search_D_income = nb_search_D_income + 1  
    return nb_search_D_income

pprint(fc_nb_search_D_income())
print("Fonction spécifique")

################################################################################

print(colored("Liste des noms, prénoms, id et revenus des personnes qui gagnent plus de 4000$", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# Méthode: Fonction de comptage et de recherche spécifique
def fc_win_money(count: int):
    income = []
    liste_income = []
    for person in people:
        income = person["income"]
        if float(income[1:]) > count :
            liste_income.append(person["id"])
            liste_income.append(person["first_name"])
            liste_income.append(person["last_name"])
            liste_income.append(person["income"])
    return liste_income

print(fc_win_money(4000))
print("Fonction générale avec argument avec liste à multiple colonne")

################################################################################

print(colored("Homme le plus riche (nom et id) :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# Méthode: Homme le plus riche
# 1 Cherche les hommes

# > init: liste "hommes"
hommes = []

# > fontion: liste les hommes
def fc_list_homme():
    list_hommes = []
    for person in people:
        if person["gender"] == "Male":
            list_hommes.append(person) 
    return list_hommes

# > résultat: liste des hommes complète du fichier
hommes = fc_list_homme()

# 2 Cherche le plus riche
# ---2.1 Cherche les salaires
# ---2.2 Cherche le salaire max
# ---2.3 Condition le salaire max chez les hommes

# > init: liste "liste homme gagne les plus"
liste_max_win_H = {}

# > fontion: Liste de l'homme le plus riche
def fc_max_win(hommes):
    liste_max = {}
    win_max = 0
    for riche in hommes:
        if float(riche["income"][1:]) > win_max:
            win_max = float(riche["income"][1:])
            liste_max = riche
    return liste_max

# > résultat: liste des hommes complète du fichier
liste_max_win_H = fc_max_win(hommes)

# 4 Retour l'id et le nom et salaire
max_win_H = liste_max_win_H["id"], liste_max_win_H["last_name"], liste_max_win_H["income"]
pprint(max_win_H)
print("Fonction générale avec argument")

################################################################################

print(colored("Salaire moyen :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# Méthode: Salaire moyen

# Fonction: liste des salaires gagné
def fc_income_win(people):
    liste_salaire = []
    for salaire in people:
        liste_salaire.append(float(salaire["income"][1:]))
    return liste_salaire

# Focntion: qui calcule la moyenne "sum()"
def fc_moyenne(liste_salaire):
    return((sum(liste_salaire) / len(liste_salaire)))

print(fc_moyenne(fc_income_win(people)))
print("Fonction générale avec argument")

################################################################################

print(colored("Salaire médian :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# Méthode: Salaire médian
# Définition: Pour calculer le salaire médian d'une population, il convient de 
# classer les salaires des actifs par ordre croissant, puis de les partager au 
# sein de deux groupes contenant le même nombre de salaires. Le salaire médian 
# correspond au salaire situé au centre de ces deux groupes.
# Il s'agit du 5eme sur un groupe de 9
# Il s'agit de la moyenne du 5 et 6 sur un groupe de 10

# 1 Récupération liste salaire
liste_salaire = fc_income_win(people)

# 2 Trie par ordre croissant: Non utile via lib
def fc_tri_croissant(tri_salaire):
    return sorted(tri_salaire)

# 3 Récupération liste salaire: Non utile via lib
liste_salaire_trie = fc_tri_croissant(liste_salaire)

# 4 Partager la liste en deux groupe de même nombre: Non utile via lib

# 5 Le salaire médian est situé entre les deux groupes
# Utilisation de la librairie statistics
print(statistics.median(liste_salaire))

################################################################################

print(colored("Nombre de personnes qui habitent dans l'hémisphère nord :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# L'hémisphère est définie par rapport:
# A l'équateur pour le Nord / Sud (qui est la l'atitude 0)
# Au méridien de Greenwich pour le Ouest / Est (qui est la 
# longétude 0)

liste_hemisphère_nord = []

def fc_list_H_North():
    list_H_North = []
    for person in people:
        if person["latitude"] >= 0:
            list_H_North.append(person) 
    return list_H_North

liste_hemisphère_nord = fc_list_H_North()

def fc_nb_search_Hemisphère(liste_hemis):
    return len(liste_hemis)

print(fc_nb_search_Hemisphère(liste_hemisphère_nord))
print("Fonction spécifique")

################################################################################

print(colored("Salaire moyen des personnes qui habitent dans l'hémisphère sud :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# Méthode: Salaire moyen

# Fonction: liste des salaires gagné
def fc_income_win(people):
    liste_salaire = []
    for salaire in people:
        if person["latitude"] <= 0:
            liste_salaire.append(float(salaire["income"][1:]))
    return liste_salaire

# Focntion: qui calcule la moyenne "sum()"
def fc_moyenne(liste_salaire):
    return((sum(liste_salaire) / len(liste_salaire)))

print(fc_moyenne(fc_income_win(people)))
print("Fonction générale avec argument")

################################################################################

print(colored('LEVEL 3' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Personne qui habite le plus près de Bérénice Cawt (nom et id) :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# Méthode:
# 1 Recherche de Bérénice Cawt
# 2 Recherche des personnes à comparer
# 3 Recherche dans la liste la distance la plus courte

# Fonction:
# I - Recherche du dictionnaire / au nom d'entrée
def fc_search_name(people: list, type1: str, valeur1: str, type2: str, valeur2: str):
    for person in people:
        if person[type1] == valeur1 and person[type2] == valeur2:
            return person
# II - Comptage du nombre (int) / à la liste de recherche
def fc_nb_scearch(people: list) -> list:
    list_people = fc_search_name(people)
    return len(list_people)
# III - Calcul de la distance (positive) / au théorème de Pythagore
def fc_distance(x1: float, y1: float, x2: float, y2: float):
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
# IV - Comparaison du nom (dictionnaire) / à la liste de personne
def fc_compare(people: list, name: dict):
    compare = []
    for person in people:
        distance = fc_distance(person["longitude"], person["latitude"], name["longitude"], name["latitude"])
        compare.append({"id": person["id"], "last_name": person["last_name"], "first_name": person["first_name"], "distance": distance})
    return compare
# V - Renvoi la distance la plus petite / à la liste des distances
def fc_distance_min(compare: list, name: dict):
    closet_name = []
    for distance in compare:
        if distance["id"] != name["id"]:
            closet_name.append(float(distance["distance"]))
    return min(closet_name) # Minimun distance
# VI - Renvoi l'id de la personne la plus proche / à la liste des distances et de la distance min
def id_distance_min(compare: list, distance_min: int):
    for distance in compare:
        if float(distance["distance"]) == distance_min:
            return distance["id"]
# VII - Renvoi du nom/Prénom et id de la personne la plus proche
def name_distance_min(compare: list, name_id: int):
    for name in compare:
        if name["id"] == name_id:
            print(name["id"], name["last_name"],name["first_name"])
# VIII - Synthèse des appels des fonctions
def min_distance(people: list, prénom: str, nom: str):
    # I - Dictionnaire avec la personne rechercher
    name_search = fc_search_name(people, "first_name", prénom, "last_name", nom)
    # II - Liste avec des personnes avec leur distance calculer
    liste_compare = fc_compare(people, name_search)
    # III - Distance la plus courte (variable int)
    closet_min = fc_distance_min(liste_compare, name_search)
    # IV - ID du nom de la personne la plus proche (variable int)
    id_name = id_distance_min(liste_compare, closet_min)
    # V - ID et Nom/Prénom de la personne la plus proche 
    print(name_search["id"], name_search["last_name"], name_search["first_name"]) 
    print(f"Distance qui sépare: {closet_min}")
    name_distance_min(liste_compare, id_name)


# Affichage du nom de la personne la plus proche:
min_distance(people, "Bérénice", "Cawt")

#################################################################################

print(colored("Personne qui habite le plus près de Ruì Brach (nom et id) :", 'yellow'))
print(colored('Fait', 'red', 'on_green'))

# Affichage du nom de la personne la plus proche:
min_distance(people, "Ruì", "Brach")

################################################################################

print(colored("les 10 personnes qui habitent les plus près de Josée Boshard (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

# Affichage du nom de la personne la plus proche:
min_distance(people, "Josée", "Boshard")

################################################################################

print(colored("Les noms et ids des 23 personnes qui travaillent chez google :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne la plus agée :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne la plus jeune :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
print(colored("Moyenne des différences d'age :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('LEVEL 4' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
print(colored("Genre de film le plus populaire :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Genres de film par ordre de popularité :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des genres de film et nombre de personnes qui les préfèrent :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des hommes qui aiment les films noirs :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des femmes qui aiment les drames et habitent sur le fuseau horaire, de Paris : ", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("""Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une
préférence de film en commun (afficher les deux et la distance entre les deux):""", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des couples femmes / hommes qui ont les même préférences de films :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('MATCH' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
"""
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui ont le plus de gouts en commun.
    Puis ceux qui sont les plus proches.
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi 
    les films d'aventure.
    La différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agé des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.                  ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction         ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum     ߷    ߷    ߷    

"""
print(colored("liste de couples à matcher (nom et id pour chaque membre du couple) :", 'yellow'))
print(colored('Exemple :', 'green'))
print(colored('1 Alice A.\t2 Bob B.'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
